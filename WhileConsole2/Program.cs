﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            int K;
            double P,S,P1;
            Console.Write("(0>P>50) P = ");
            P = int.Parse(Console.ReadLine());
            if (P <= 0 || P>=50)
            {
                Console.WriteLine("Error! Must be 0>P>50!");
                Console.ReadLine();
                return;
            }
            S = 10;
            K=1;
            P1 = S / 100 * P;
            while(S<=200)
            {
                P1 = S / 100 * P;
                S = S + P1;
                K++;
            }
            Console.WriteLine("Days = {0}", K);
            Console.WriteLine("S = {0}", S);
            Console.ReadLine();
        }
    }
}
