﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tusk5Console
{
    class Program
    {
        static void Main(string[] args)
        {
            double x;
            double y;
            double z;
            double t;
            Console.WriteLine("x =");
            x = double.Parse(Console.ReadLine());
            Console.WriteLine("y =");
            y = double.Parse(Console.ReadLine());
            Console.WriteLine("z =");
            z = double.Parse(Console.ReadLine());
            t = ((2 * Math.Cos(x - (Math.PI) / 6)) / (0.5 + Math.Pow(Math.Sin(y), 2))) * ((1 + (Math.Pow(z, 2)) / (3 - Math.Pow(z, 2) / 5)));
            Console.WriteLine("t = {0}",t);
            Console.ReadLine();
        }
    }
}
