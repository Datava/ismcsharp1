﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadEquationConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            double b;
            double c;
            double d;
            double x;
            double x1;
            double x2;
            Console.WriteLine("a =");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("b =");
            b = double.Parse(Console.ReadLine());
            Console.WriteLine("c =");
            c = double.Parse(Console.ReadLine());
            d = b * b - 4 * a * c;
            if (d > 0)
            {
                x1 = (-b - Math.Sqrt(d)) / (2 * a);
                x2 = (-b + Math.Sqrt(d)) / (2 * a);
                Console.WriteLine("x1 = {0} ; x2 = {1}", x1,x2);
            }
            else
                if (d == 0)
                {
                    x = -b / (2 * a);
                    Console.WriteLine("x = {0}", x);
                }
                else
                    Console.WriteLine("Solution does not exist");
            Console.ReadLine();


        }
    }
}
