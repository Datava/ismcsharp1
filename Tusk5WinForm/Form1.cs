﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tusk1WinForm5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x;
            double y;
            double z;
            double t;
            x = double.Parse(textBox1.Text);
            y = double.Parse(textBox2.Text);
            z = double.Parse(textBox3.Text);
            t = ((2 * Math.Cos(x - (Math.PI) / 6)) / (0.5 + Math.Pow(Math.Sin(y), 2))) * ((1 + (Math.Pow(z, 2)) / (3 - Math.Pow(z, 2) / 5)));
            textBox4.Text = t.ToString();
        }
    }
}
