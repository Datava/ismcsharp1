﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tusk1Console3
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            double b;
            double x;
            double y;
            Console.WriteLine("a =");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("b =");
            b = double.Parse(Console.ReadLine());
            Console.WriteLine("x =");
            x = double.Parse(Console.ReadLine());
            y = 2.4 * Math.Abs((Math.Pow(x, 2) + b) / a) + (a - b) * Math.Pow(Math.Sin(a - b), 2) + Math.Pow(10, -2) * (x - b);
            Console.WriteLine("y = {0}", y);
            Console.ReadLine();
        }
    }
}
