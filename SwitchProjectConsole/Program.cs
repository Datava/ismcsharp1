﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchProjectConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int v = 0;
            do
            {
                int day, month, x, numbers;
                double a, b, res;
                Console.WriteLine("1. Deciphering the day of the week");
                Console.WriteLine("2. Deciphering the month");
                Console.WriteLine("3. Perform operations with numbers");
                Console.WriteLine("4. Exit.");
                x = int.Parse(Console.ReadLine());
                switch (x)
                {
                    case 1:
                        Console.WriteLine("Enter the number of days of the week");
                        day = int.Parse(Console.ReadLine());
                        switch (day)
                        {
                            case 1:
                                Console.WriteLine("Monday");
                                break;
                            case 2:
                                Console.WriteLine("Tuesday");
                                break;
                            case 3:
                                Console.WriteLine("Wednesday");
                                break;
                            case 4:
                                Console.WriteLine("Thursday");
                                break;
                            case 5:
                                Console.WriteLine("Friday");
                                break;
                            case 6:
                                Console.WriteLine("Saturday");
                                break;
                            case 7:
                                Console.WriteLine("Sunday");
                                break;
                            default:
                                Console.WriteLine("Error. Number must be from 1 to 7");
                                break;
                        }
                        break;
                    case 2:
                        Console.WriteLine("Enter the number of month");
                        month = int.Parse(Console.ReadLine());
                        switch (month)
                        {
                            case 1:
                                Console.WriteLine("January");
                                break;
                            case 2:
                                Console.WriteLine("February");
                                break;
                            case 3:
                                Console.WriteLine("March");
                                break;
                            case 4:
                                Console.WriteLine("April");
                                break;
                            case 5:
                                Console.WriteLine("May");
                                break;
                            case 6:
                                Console.WriteLine("June");
                                break;
                            case 7:
                                Console.WriteLine("July");
                                break;
                            case 8:
                                Console.WriteLine("August");
                                break;
                            case 9:
                                Console.WriteLine("September");
                                break;
                            case 10:
                                Console.WriteLine("October");
                                break;
                            case 11:
                                Console.WriteLine("November");
                                break;
                            case 12:
                                Console.WriteLine("December");
                                break;
                            default:
                                Console.WriteLine("Error. Number must be from 1 to 12");
                                break;
                        }
                        break;
                    case 3:
                        Console.WriteLine("Enter the a");
                        a = double.Parse(Console.ReadLine());
                        Console.WriteLine("Enter the b");
                        b = double.Parse(Console.ReadLine());
                        Console.WriteLine("1. Addition");
                        Console.WriteLine("2. Multiplication");
                        Console.WriteLine("3. Subtraction");
                        Console.WriteLine("4. Division");
                        numbers = int.Parse(Console.ReadLine());
                        switch (numbers)
                        {
                            case 1:
                                res = a + b;
                                Console.WriteLine("Result {0}", res);
                                break;
                            case 2:
                                res = a * b;
                                Console.WriteLine("Result {0}", res);
                                break;
                            case 3:
                                res = a - b;
                                Console.WriteLine("Result {0}", res);
                                break;
                            case 4:
                                res = a / b;
                                Console.WriteLine("Result {0}", res);
                                break;
                            default:
                                Console.WriteLine("Error. Numberof menu must be from 1 to 4");
                                break;
                        }
                        break;
                    case 4:
                        return;
                        break;
                    default:
                        Console.WriteLine("Error. Number of menu must be from 1 to 4");
                        break;
                }
            }
            while (v == 0);
        }
    }
}
