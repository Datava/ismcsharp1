﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuadEquationWinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label6.Visible = false;
            textBox6.Visible = false;
            label5.Visible = false;
            textBox5.Visible = false;
            label4.Visible = false;
            textBox4.Visible = false;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a;
            double b;
            double c;
            double d;
            double x;
            double x1;
            double x2;
            a = double.Parse(textBox1.Text);
            b = double.Parse(textBox2.Text);
            c = double.Parse(textBox3.Text);
            d = b * b - 4 * a * c;
            if (d>0)
            {
                label5.Visible = true;
                textBox5.Visible = true;
                label4.Visible = true;
                textBox4.Visible = true;
                label6.Visible = false;
                textBox6.Visible = false;
                x1 = (-b - Math.Sqrt(d)) / (2 * a);
                x2 = (-b + Math.Sqrt(d)) / (2 * a);
                textBox4.Text = x1.ToString();
                textBox5.Text = x2.ToString();

            }
            else
                if (d == 0)
                {
                    label5.Visible = false;
                    textBox5.Visible = false;
                    label4.Visible = false;
                    textBox4.Visible = false;
                    label6.Visible = true;
                    textBox6.Visible = true;
                    x = -b / (2 * a);
                    textBox6.Text = x.ToString();
                }
                else
                {
                    label6.Visible = false;
                    textBox6.Visible = false;
                    label5.Visible = false;
                    textBox5.Visible = false;
                    label4.Visible = false;
                    textBox4.Visible = false;
                    MessageBox.Show("Solution does not exist");
                }
             
        }
    }
}
