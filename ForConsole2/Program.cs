﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            int N,a;
            double x,res;
            Console.Write("(N>0) N = ");
            N = int.Parse(Console.ReadLine());
            if (N <= 0)
            {
                Console.WriteLine("Error! Must be N<0!");
                Console.ReadLine();
                return;
            }
            for (a = 1,x=1,res=0; x <= N; x++)
                res = a / x + res;
            Console.WriteLine("{0}", res);
            Console.ReadLine();
        }
    }
}
