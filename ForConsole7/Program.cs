﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole7
{
    class Program
    {
        static void Main(string[] args)
        {
            uint N, K;
            double x, res;
            Console.Write("N = ");
            N = uint.Parse(Console.ReadLine());
            Console.Write("K = ");
            K = uint.Parse(Console.ReadLine());
            for (x = 1, res = 0; x <= N; x++)
                res = Math.Pow(x, N) + res;
            Console.WriteLine("{0}", res);
            Console.ReadLine();
        }
    }
}
