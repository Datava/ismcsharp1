﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhileConsole1
{
    class Program
    {
        static void Main(string[] args)
        {
            int x=0;
            do
            {
                int a, b, res;
                a = int.Parse(Console.ReadLine());
                b = int.Parse(Console.ReadLine());
                res = a + b;
                Console.WriteLine(res);
            }
            while (x != 0);
            Console.ReadLine();
        }
    }
}
