﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole3
{
    class Program
    {
        static void Main(string[] args)
        {
            double A,x,res;
            int N;
            Console.Write("A = ");
            A = double.Parse(Console.ReadLine());
            Console.Write("(N>0) N = ");
            N = int.Parse(Console.ReadLine());
            if(N<=0)
            {
                Console.WriteLine("Error! Must be N>0!");
                Console.ReadLine();
                return;
            }
            for (res = A,x=1; x < N; x++)
                res = res * A;
            Console.WriteLine("{0}", res);
            Console.ReadLine();
        }
    }
}
