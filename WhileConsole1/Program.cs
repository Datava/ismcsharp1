﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileConsole1
{
    class Program
    {
        static void Main(string[] args)
        {
            int N, K;
            Console.Write("(N>0) N = ");
            N = int.Parse(Console.ReadLine());
            if (N <= 0)
            {
                Console.WriteLine("Error! Must be N>0!");
                Console.ReadLine();
                return;
            }
            K=N;
            while (Math.Pow(3, K) > N)
                K = K - 1;
            Console.WriteLine("K = {0}", K+1);
            Console.ReadLine();

        }
    }
}
