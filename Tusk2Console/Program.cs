﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tusk2Console
{
    class Program
    {
        static void Main(string[] args)
        {
            double m;
            double n;
            double z1;
            double z2;
            Console.WriteLine("m= ");
            m = double.Parse(Console.ReadLine());
            Console.WriteLine("n= ");
            n = double.Parse(Console.ReadLine());
            z1 = ((m - 1) * Math.Sqrt (m) - (n - 1)*Math.Sqrt(n)) / (Math.Sqrt(Math.Pow(m,3) * n) + n * m + Math.Pow(m,2) - m);
            z2 = (Math.Sqrt(m) - Math.Sqrt(n)) / m;
            Console.WriteLine("z1 = {0} ; z2 = {1} " , z1 , z2);
            Console.ReadLine();
        }
    }
}
