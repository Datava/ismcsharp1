﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tusk1WPF5
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double x;
            double y;
            double z;
            double t;
            x = double.Parse(Tx.Text);
            y = double.Parse(Ty.Text);
            z = double.Parse(Tz.Text);
            t = ((2 * Math.Cos(x - (Math.PI) / 6)) / (0.5 + Math.Pow(Math.Sin(y), 2))) * ((1 + (Math.Pow(z, 2)) / (3 - Math.Pow(z, 2) / 5)));
            Tt.Text = t.ToString();
        }
    }
}
