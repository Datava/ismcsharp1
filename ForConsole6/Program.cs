﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole6
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            double x, res;
            Console.Write("(N>0) N = ");
            N = int.Parse(Console.ReadLine());
            if (N <= 0)
            {
                Console.WriteLine("Error! Must be N>0!");
                Console.ReadLine();
                return;
            }
            for(x=1,res=0;N!=1;x++,N--)
                res = Math.Pow(x, N) + res;
            Console.WriteLine("{0}",res+x);
            Console.ReadLine();


        }
    }
}
