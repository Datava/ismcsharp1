﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tusk1Console
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            double b;
            double res;
            Console.WriteLine("a= ");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("b= ");
            b = double.Parse(Console.ReadLine());
            res = a * 2 + b * 2;
            Console.WriteLine("res = {0} ",res);
            Console.ReadLine();
        }
    }
}
