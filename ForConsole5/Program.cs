﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole5
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            float x,y, res,res1;
            Console.WriteLine("N>0");
            Console.Write("N = ");
            N=int.Parse(Console.ReadLine());
            if (N <= 0)
            {
                Console.WriteLine("Error! Must be N>0!");
                Console.ReadLine();
                return;
            }
            for (y = 1,res1=0; y <= N; y++)
            {
                for (x = 1F, res = 1; x <= y; x++)
                {
                    res = x * res;
                }
                res1 = res + res1;
            }
            Console.WriteLine("{0}", res1);
            Console.ReadLine();

        }
    }
}
