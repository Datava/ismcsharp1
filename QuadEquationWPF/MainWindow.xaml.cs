﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuadEquationWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Lx1.Visibility = Visibility.Hidden;
            Tx1.Visibility = Visibility.Hidden;
            Lx2.Visibility = Visibility.Hidden;
            Tx2.Visibility = Visibility.Hidden;
            Lx.Visibility = Visibility.Hidden;
            Tx.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            double a;
            double b;
            double c;
            double d;
            double x;
            double x1;
            double x2;
            a = double.Parse(Ta.Text);
            b = double.Parse(Tb.Text);
            c = double.Parse(Tc.Text);
            d = b * b - 4 * a * c;
            if (d > 0)
            {
                Lx1.Visibility = Visibility.Visible;
                Lx2.Visibility = Visibility.Visible;
                Tx2.Visibility = Visibility.Visible;
                Tx1.Visibility = Visibility.Visible;
                Lx.Visibility = Visibility.Hidden;
                Tx.Visibility = Visibility.Hidden;
                x1 = (-b - Math.Sqrt(d)) / (2 * a);
                x2 = (-b + Math.Sqrt(d)) / (2 * a);
                Tx1.Text = x1.ToString();
                Tx2.Text = x2.ToString();

            }
            else
                if (d == 0)
                {
                    Lx1.Visibility = Visibility.Hidden;
                    Tx1.Visibility = Visibility.Hidden;
                    Lx2.Visibility = Visibility.Hidden;
                    Tx2.Visibility = Visibility.Hidden;
                    Tx.Visibility = Visibility.Visible;
                    Lx.Visibility = Visibility.Visible;
                    x = -b / (2 * a);
                    Tx.Text = x.ToString();
                }
                else
                {
                    Lx1.Visibility = Visibility.Hidden;
                    Tx1.Visibility = Visibility.Hidden;
                    Lx2.Visibility = Visibility.Hidden;
                    Tx2.Visibility = Visibility.Hidden;
                    Lx.Visibility = Visibility.Hidden;
                    Tx.Visibility = Visibility.Hidden;
                    MessageBox.Show("Solution does not exist");
                }
        }
    }
}
