﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole4
{
    class Program
    {
        static void Main(string[] args)
        {
            int A, B;
            double res, x;
            Console.WriteLine("A < B");
            Console.Write("A = ");
            A = int.Parse(Console.ReadLine());
            Console.Write("B = ");
            B = int.Parse(Console.ReadLine());
            if(A>=B)
            {
                Console.WriteLine("Error! Must be A<B!");
                Console.ReadLine();
                return;
            }
            for (x=A,res=0;x<=B;x++)
            {
                res = x * x+res;
            }
            Console.WriteLine("{0}", res);
            Console.ReadLine();
        }
    }
}
